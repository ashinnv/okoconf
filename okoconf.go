package okoconf

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
	//"github.com/kylelemons/go-gypsy/yaml"
)

/*
	Each stage of the pipeline is defined by an input address (ie. "localhost: 8043"),
	an executable (ie. "classifier"), and output(ie. "localhost:8044"). This leads into
	another stage (ie. "localhost:8044,stampHandler,example.com:8045" if the next step
	of the pipeline is stampHandler which sends off to another process on example.com:8045).
*/
type stage struct {
	Input      string
	Outpt      string
	Executable string
}

type cam struct {
	//Motion detection options
	pixelChangeThreshold int //Interger amount that a pixel color channel needs to change to be considered different in motion detection
	pixelChangePercent   int //Interger percent of an image's pixels that need to change against a reference frame to count as motion

	ColorMode string
	RotatValu int //Degrees to rotate(counter-clockwise)

	CameraIndex string //V4L index
}

type ConfBucket struct {

	//User config options
	UserName string
	PassHash string //Bcrypt hash of password

	//Camera list
	CamList []cam

	//Define pipeline stages
	Stages []stage
}

func Initl(configStruct *ConfBucket) {

	//List of possible config locations. First one found is the config we go with.
	targList := []string{"./oko.yaml", "/etc/oko/oko.yaml"}

	fLoc := ""
	for _, fName := range targList {
		if checkFile(fName) {
			fLoc = fName
			break
		}
	}

	if fLoc == "" {
		fmt.Println("Could not find a config file. Locations searched: ", targList)
		os.Exit(0)
	} else {
		fmt.Printf("Got config file location. Using %s", fLoc)
	}

	strDat, err := ioutil.ReadFile(fLoc)
	//if err != nil {
	fmt.Println("Error was:", err)
	//}

	yaml.Unmarshal(strDat, &configStruct)
}

func checkFile(input string) bool {
	fmt.Println("WARNING! CHECKFILE IS NOT IMPLEMENTED.")
	return true
}
